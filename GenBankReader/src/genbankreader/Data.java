/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.util.ArrayList;

/**
 *
 * @author Dani2
 */
public class Data {
    private ArrayList<String> organisms = new ArrayList();
    private ArrayList<String> db_xrefs = new ArrayList();
    private ArrayList<String> geneCoords = new ArrayList();
    private ArrayList<String> geneNames = new ArrayList();
    private ArrayList<String> cdsCoords = new ArrayList();
    private ArrayList<String> cdsProduct = new ArrayList();
    private ArrayList<String> proteinTranslation= new ArrayList();
    private ArrayList<String> sequences = new ArrayList();
    
    
    public Data(ArrayList<ArrayList<String>> data){
        
        try{
            organisms = data.get(0);
            db_xrefs = data.get(1);
            geneCoords = data.get(2);
            geneNames = data.get(3);
            cdsCoords = data.get(4);
            cdsProduct = data.get(5);
            proteinTranslation = data.get(6);
            sequences = data.get(7);
            
            
        }catch(Exception e){
            System.out.println("Something has gone wrong");
        }
        
    }
    
    
    public ArrayList<String> getOrganisms() {
        return organisms;
    }

    public void setOrganisms(ArrayList<String> organisms) {
        this.organisms = organisms;
    }

    public ArrayList<String> getDb_xrefs() {
        return db_xrefs;
    }

    public void setDb_xrefs(ArrayList<String> db_xrefs) {
        this.db_xrefs = db_xrefs;
    }

    public ArrayList<String> getGeneCoords() {
        return geneCoords;
    }

    public void setGeneCoords(ArrayList<String> geneCoords) {
        this.geneCoords = geneCoords;
    }

    public ArrayList<String> getGeneNames() {
        return geneNames;
    }

    public void setGeneNames(ArrayList<String> geneNames) {
        this.geneNames = geneNames;
    }

    public ArrayList<String> getCdsCoords() {
        return cdsCoords;
    }

    public void setCdsCoords(ArrayList<String> cdsCoords) {
        this.cdsCoords = cdsCoords;
    }

    public ArrayList<String> getCdsProduct() {
        return cdsProduct;
    }

    public void setCdsProduct(ArrayList<String> cdsProduct) {
        this.cdsProduct = cdsProduct;
    }

    public ArrayList<String> getProteinTranslation() {
        return proteinTranslation;
    }

    public void setProteinTranslation(ArrayList<String> proteinTranslation) {
        this.proteinTranslation = proteinTranslation;
    }

    public ArrayList<String> getSequences() {
        return sequences;
    }

    public void setSequences(ArrayList<String> sequences) {
        this.sequences = sequences;
    }
    
}

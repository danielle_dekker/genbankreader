/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;
/**
 *
 * @author Dani2
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Reads in a File
 * returns the file read as an ArrayList
 * @author Dani2
 */
public class FileReader {

    /**
     * Reads in a file by line
     * @param filePath
     * @return ArrayList with lines 
     */
    public ArrayList ReadFile(String filePath) {
        ArrayList<String> data = new ArrayList<>();
        
	try (BufferedReader br = new BufferedReader(new java.io.FileReader(filePath))){
            String sCurrentLine;
            
            while ((sCurrentLine = br.readLine()) != null) {
               data.add(sCurrentLine);
            }
            return data;
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println("this file cannot be found/opened");
	} 
        return data;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.util.ArrayList;

/**
 *
 * @author Dani2
 */
public class FileParser {
    
    private ArrayList<String[]> cleanData = new ArrayList();
    
     
    /**
     * Parses the ArrayList with data provided
     * @param data 
     * @return ArrayList with all features found
     */
    public ArrayList<ArrayList<String>> parse(ArrayList<String> data){
        FileParser fp = new FileParser();
        
        ArrayList<ArrayList<String>> bigList = new ArrayList();
        ArrayList<String> organisms = new ArrayList();
        ArrayList<String> db_xrefs = new ArrayList();
        ArrayList<String> geneCoords = new ArrayList();
        ArrayList<String> geneNames = new ArrayList();
        ArrayList<String> cdsCoords = new ArrayList();
        ArrayList<String> cdsProduct = new ArrayList();
        ArrayList<String> proteinTranslation= new ArrayList();
        ArrayList<String> sequences = new ArrayList();
        
        String s;
        int counter = 0;
        int geneCounter = 0;
        int dbCounter = 0;
        String sequenceBuilder;
        
        
        for(int i = 0; i< data.size(); i++){
            s = data.get(i);
            s = s.replaceAll("\\s","");                     //remove whitespaces
           
            if(s.matches("(\\/organism)(.)*")){             //organism
                String organism = fp.clearJunk(s);                
                organisms.add(organism);
            }
            if(s.matches("(/db_xref=\")(.)*(\")")){         //db_xref
                dbCounter += 1;
                if(dbCounter >= 2){
                    String db = fp.clearJunk(s);
                    db_xrefs.add(db);
               }
            }
            if(s.matches("(gene)(\\d)*(..)(\\d)*")){        //gene coordinates
               String geneCoordinates = s.replaceAll("gene","");
               geneCoords.add(geneCoordinates);
            }
            if(s.matches("(genecomplement)(\\()(\\d)+(..)(\\d)+(\\))")){        //gene coordinates
               String geneCoordinates = s.replaceAll("genecomplement\\(","");
               geneCoordinates = geneCoordinates.replace(")", "");
               geneCoords.add(geneCoordinates);
            }
            
            if(s.matches("(/gene=\")(.)*(\")")){            //gene name
                geneCounter += 1;   // always 2 gene names after each other
                if(geneCounter %2 == 0){
                    String geneName = fp.clearJunk(s);
                    geneNames.add(geneName);
                }
            }
            if(s.matches("(CDS)(\\d)*(.){2}(\\d)*")){       //CDS coordinates
               String CDSCoordinates = s.replaceAll("CDS", "");
               cdsCoords.add(CDSCoordinates);
               
            }
            if(s.matches("(CDScomplement)(\\()(\\d)*(.){2}(\\d)*(\\))")){       //CDS coordinates
               String CDSCoordinates = s.replaceAll("CDScomplement\\(", "");
               CDSCoordinates = CDSCoordinates.replaceAll("\\)","");
               cdsCoords.add(CDSCoordinates);
               
            }
            
            if(s.matches("(/product=\")(.)*(\")")){       //CDS product
                String product = fp.clearJunk(s);
                cdsProduct.add(product);
                
            }
            if(s.matches("(/translation=\")(\\w)*")){       //translation
               String translation = combineTranslationLine(data,s,i);
               translation = fp.clearJunk(translation);
               proteinTranslation.add(translation);
               
            }
            
             
            if(s.matches("(\\d*)[actg]*")){                 //Sequence
               counter +=1; 
               if(counter == 1){ // get only the first line
                sequenceBuilder = makeSequence(data,i);
                String sequence = removeDigitsAndWhitespaces(sequenceBuilder);
                sequences.add(sequence);
               }
            } 
        }
        bigList.add(organisms);
        bigList.add(db_xrefs);
        bigList.add(geneCoords);
        bigList.add(geneNames);
        bigList.add(cdsCoords);
        bigList.add(cdsProduct);
        bigList.add(proteinTranslation);
        bigList.add(sequences);
        
       return bigList;
    }
    
    /**
     * replaces all letters
     * @param anything
     * @return 
     */
     private String clearJunk(String anything){
        String clear;
        clear = anything.replaceAll("/[a-zA-Z]*_*[a-zA-Z]*=", "");
        clear = clear.replaceAll("\"","");
        
        return clear;
    }
     /**
     * combines all translated lines, removes whitespace
     * @param translation
     * @param loc
     * @return a string with protein translation 
     */
    private String combineTranslationLine(ArrayList<String> data,String translation,int loc){
            for(int i = 0; i< data.size()-loc;i++){
                if(data.get(loc+i).matches("(\\s)*[ACDEFGHIKLMNPRSTVWY]*")){
                    translation += data.get(loc+i);
                }
            }
       return translation.replaceAll("\\s","");
    }
    
    /**
     * Combines all the lines for the nucleotide sequence
     * @param line
     * @return a string with sequence 
     */
    private String makeSequence(ArrayList<String> data,int line){
       String localsequence = "";
       for(int i=0; i < (data.size()-line);i++){
           localsequence += data.get(line + i);
       }
       
        return localsequence;
    }
    
    /**
     * Removes all digits and white spaces
     * @param string
     * @return string without digits and spaces
     */
    public String removeDigitsAndWhitespaces(String string){
        string = string.replaceAll("\\s", "");
        string = string.replaceAll("\\d", "");
        string = string.replaceAll("/","");
        
        return string;
    }
    
    
}

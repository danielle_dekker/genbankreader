/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.util.ArrayList;

/**
 * 
 * @author Dani2
 */
public class FeatureFinder {
    
    /**
     * Finds features in the genBank file for provided coordinates
     * @param d
     * @param coordinates 
     */
    public void findFeatures(Data d,String coordinates){
        int[] coords = clearCoordinates(coordinates); //given coordinates
        
        ArrayList<Integer> cdsLocations = findLocations(d.getCdsCoords(),coords);
        ArrayList<Integer> geneLocations = findLocations(d.getGeneCoords(),coords);
        getFeatures(d,cdsLocations,geneLocations);
        
        
       
    }
    /**
     * performs all other actions needed from the class;
     * Transfer ArrayList<String> to ArrayList<Integer>
     * Divide array into 2 arrays
     * Compare given coordinates with file coordinates
     * Get locations for features
     * @param array
     * @return array list integer with locations
     */
    private ArrayList<Integer> findLocations(ArrayList<String> array,int[] coords){
        ArrayList<Integer> arrayCoordinates = getCoordinatesFromArray(array); //<- transfers ArrayList<String> to ArrayList<Integer>
        ArrayList<ArrayList<Integer>> dividedArrays = divideArrays(arrayCoordinates); //<-divides into 2 arrays 
        ArrayList<ArrayList<Integer>> locations = compareCoordinates(dividedArrays.get(0),dividedArrays.get(1),coords[0],coords[1]); // compare coordinates
        ArrayList<Integer> finalLocations = getCommonDigits(locations.get(0),locations.get(1)); // gets only true locations
        return finalLocations;
    }
    
    /**
     * Prints the Features
     * @param d
     * @param locations 
     */
    private void getFeatures(Data d,ArrayList<Integer> cdsLocations, ArrayList<Integer> geneLocations){
        // print some features!!
           for(int i: cdsLocations){
                System.out.println("CDS Product(s): " + d.getCdsProduct().get(i));
                System.out.println("CDS Coordinates: " + d.getCdsCoords().get(i));
                System.out.println("Database Xreferences: "+d.getDb_xrefs().get(i));
                System.out.println("Protein translation(s): " + d.getProteinTranslation().get(i));
            }
            for(int j: geneLocations){
                System.out.println("Gene Name(s): "+ d.getGeneNames().get(j));
                System.out.println("Gene Coordinates: "+ d.getGeneCoords().get(j));
            } 
        
    } 
            
    /**
     * Divides an integer array into 2 separate integer arrays
     * star positions come first!
     * returns 2 arrayLists within another ArrayList
     * @param positions
     * @return ArrayList<ArrayList<Integer>>
     */
    private ArrayList<ArrayList<Integer>> divideArrays(ArrayList<Integer> positions){
        ArrayList<ArrayList<Integer>> combine = new ArrayList();
        ArrayList<Integer> startPositions = new ArrayList();
        ArrayList<Integer> stopPositions = new ArrayList();
        
        for(int i = 0; i< positions.size();i+=2){   //divide the locations into two seperate arrays
            startPositions.add(positions.get(i));
        }
        for(int i = 1; i< positions.size();i+=2){
            stopPositions.add(positions.get(i));
        }
        combine.add(startPositions);
        combine.add(stopPositions);
        return combine;
    }
    
    /**
     * Checks if single integers are within their corresponding integer arrayLists
     * returns 2 integer arrayLists with their locations in an array
     * @param startPositions
     * @param stopPositions
     * @param startCoordinate
     * @param stopCoordinate
     * @return ArrayList<ArrayList<Integer>> locations
     */
    private ArrayList<ArrayList<Integer>> compareCoordinates(ArrayList<Integer> startPositions,ArrayList<Integer> stopPositions,int startCoordinate,int stopCoordinate){
        ArrayList<ArrayList<Integer>> combine = new ArrayList();
        ArrayList<Integer> startLocations = new ArrayList();
        ArrayList<Integer> stopLocations = new ArrayList();
        
        for(int x = 0;x<startPositions.size();x++){     //check to see if file coordinates are within given coordinates
            if(startPositions.get(x) >= startCoordinate){
                startLocations.add(x);
            }
        }
        for(int x = 0;x<stopPositions.size();x++){
            if(stopPositions.get(x) <= stopCoordinate){
                stopLocations.add(x);
            }
        }
        combine.add(startLocations);
        combine.add(stopLocations);
        
        return combine;
    }
    
    /**
     * Gets common numbers from 2 array lists
     * @param list
     * @param list2
     * @return integer arrayList with the common numbers 
     */
    private ArrayList<Integer> getCommonDigits(ArrayList<Integer> list1, ArrayList<Integer> list2){
        ArrayList<Integer> locations = new ArrayList();
        for(int x: list1){
            for(int y: list2){
                if(x == y){
                    locations.add(x);
                }
            }
        }
        return locations;
    }
    
    
    /**
     * Converts ArrayList of string coordinates ->(12..13) into an array of integers
     * size of the integer array is twice as long as the original ArrayList
     * @param coordinates
     * @return integer[] 
     */
    private ArrayList<Integer> getCoordinatesFromArray(ArrayList<String> coordinates){
        ArrayList<Integer> array = new ArrayList();
        
        for(int i = 0; i<coordinates.size();i++){
            int[] startStop = clearCoordinates(coordinates.get(i));
            array.add(startStop[0]);
            array.add(startStop[1]);
        }
        return array;
    }
    
    /**
     * deletes all letters/underscores/spaces and "<",">"
     * @param coordinates
     * @return String coordinates
     */
    private int[] clearCoordinates(String coordinates){
        coordinates = coordinates.replaceAll("fetch_features","");
        coordinates = coordinates.replaceAll("(<)*(>)*","");
        String[] clearCoordinates = coordinates.split("(\\..)");
        int[] ints = new int[2];
        
        try{
            ints[0] = String2Int(clearCoordinates[0]);
            ints[1] = String2Int(clearCoordinates[1]);
            return ints;
            
        }catch(Exception e){
            System.out.println("Something has gone wrong");
            System.out.println("Please provide your coordinates, for example like: <1..206>");
            return ints;
        }            
       
    } 
    
     /**
     * Converts a String digit to an integer
     * @param digit
     * @return integer 
     */
    private int String2Int(String digit){
        int number = Integer.parseInt(digit);
        return number;
    }
}

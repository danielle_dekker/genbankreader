/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;


/**
 *
 * @author Dani2
 */
public class FastaWriter {
    
    /**
     * Writes a file for the given sequence 
     * in Fasta format
     * Asks if you want a fasta file
     * @param sequence
     * @param ID 
     */
    public void write(String sequence, String ID){
        String [] sequences = sequence.split("(?<=\\G.{60})");
        System.out.println(">"+ID);
        for(String s: sequences){
            System.out.println(s);
        }
        
        String filepath = "";
        int answer = JOptionPane.showConfirmDialog(null, "Would you like to write a fasta file?");
        if(answer == 0){
            String s = (String)JOptionPane.showInputDialog("Please provide your output path");
            filepath = s;
        }
        if(!filepath.matches("no")){
            writeFile(ID,sequence,filepath);
        }
        
        
    }
    /**
     * Writes a file with fasta format
     * @param ID
     * @param sequence
     * @param outputPath 
     */
    private void writeFile(String ID, String sequence,String outputPath){
        String [] sequences = sequence.split("(?<=\\G.{60})");
        
        try{
            File file = new File(outputPath);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(">" + ID + "\n");
                
            for(String s : sequences){
                writer.write(s+"\n");
                System.out.println(s);
                
            }
            
            writer.close();
            
        }catch(IOException e){
            e.printStackTrace();
            System.out.println("Could not write file");
        }
    }
    
    
    
}

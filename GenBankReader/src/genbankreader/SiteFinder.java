/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.util.ArrayList;

/**
 *
 * @author Dani2
 */
public class SiteFinder {
    public Data data;
    
    public void findSites(Data d, String site){
        this.data = d;
        String sequence = clearSite(site);
        String type = determineType(sequence);
        
        if(type.matches("nucleotide")){
            ArrayList<Integer> matchLocations = handleNucleotides(sequence);
            printNucleotideMatch(matchLocations);
        }
        if(type.matches("protein")){
           handleAminoAcids(sequence);
        }
    }
    
    /**
     * Handles an amino acid
     * Finds any matches in the file's amino acids
     * returns the locations in an integer array list
     * @param sequence
     * @return ArrayList of integers
     */
    private void handleAminoAcids(String sequence){
        ArrayList<Integer> locations;
        sequence = sequence.toUpperCase();
        String fullSequence;
        
        for(int i = 0; i< data.getProteinTranslation().size();i++){
            
            fullSequence = data.getProteinTranslation().get(i);
           
            locations = getMatchLocations(fullSequence,sequence);
            if(!locations.isEmpty()){
                
                printAminoAcidMatch(locations,i);
            }
        }
    }
   private void printAminoAcidMatch(ArrayList<Integer> matchLocations,int cdsLocation){
       for(int i=0; i<matchLocations.size();i+=2){
                System.out.println("Your sequence: "+ data.getProteinTranslation().get(cdsLocation).substring(matchLocations.get(i),matchLocations.get(i+1))+
                        " has been found at: "+ matchLocations.get(i) + " to "+ matchLocations.get(i+1));
                System.out.println("The matching amino acid sequence : "+ data.getProteinTranslation().get(cdsLocation));
                System.out.println("The matching cds product: "+ data.getCdsProduct().get(cdsLocation));
                
        }
       
   }
   
    /**
     * prints the nucleotide match
     * @param matchLocations 
     */
    private void printNucleotideMatch(ArrayList<Integer> matchLocations){
        for(int i=0; i<matchLocations.size();i+=2){
                System.out.println("A match has been found at: "+ matchLocations.get(i) + " to "+ matchLocations.get(i+1));
                System.out.println("The matching sequence: "+ data.getSequences().get(0).substring(matchLocations.get(i) , matchLocations.get(i+1)));
        }
        
    }
    
    /**
     * Handles a nucleotide sequence
     * Searches for the sequence within the given file's sequence
     * If found gives it location and prints any features if they are within the sequence's locations
     * returns the locations found
     * @param sequence 
     * @return ArrayList of integer locations
     */
    private ArrayList<Integer> handleNucleotides(String sequence){
        String locations;
        FeatureFinder ff = new FeatureFinder();
        
        ArrayList<Integer> nucleotideMatchLocations = getMatchLocations(data.getSequences().get(0),sequence);
            for(int i= 0; i< nucleotideMatchLocations.size();i++){
                locations = Integer.toString(nucleotideMatchLocations.get(0))+".."+Integer.toString(nucleotideMatchLocations.get(1));
                ff.findFeatures(data, locations);
            }
        return nucleotideMatchLocations;
    }
    
    
    
    /**
     * Looks for a sequence within the other sequence
     * if found; returns the locations in arrayList with locations
     * @param fullSequence
     * @param sequence
     * @param positions empty array !! for positions to be put
     * @return ArrayList with integer locations
     */
    private ArrayList<Integer> getMatchLocations(String fullSequence, String sequence){
        int length = sequence.length();
        ArrayList<Integer> positions = new ArrayList();
        
        for(int i = 0; i< (fullSequence.length()-length) ;i++){
           String tinySequence = fullSequence.substring(i,(i+length));
           if(tinySequence.matches(sequence)){
               positions.add(i);
               positions.add((i+length));
           }
        }
        
       return positions;
    }
    
    
    /**
     * Checks if it's a nucleotide or amino acid sequence
     * @param sequence
     * @return string type 
     */
    private String determineType(String sequence){
        String type = "" ;
        if(sequence.matches("[actg]+")){
            type = "nucleotide";
        }
        else if(sequence.matches("[acdefghiklmnprstvwy]+")){
            type = "protein";
        }else{
            System.out.println("your sequence has not been recognized!");
        }
        return type;
    }
    
    /**
     * clears some text and "< >"
     * @param site
     * @return clear string
     */
    private String clearSite(String site){
        site = site.replaceAll("find_sites","");
        site = site.replaceAll("(<)*(>)*","");
        site = site.toLowerCase();
        return site;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

import java.util.ArrayList;

/**
 *
 * @author Dani2
 */
public class Console {
    public ArrayList<String> data;
    public ArrayList<ArrayList<String>> completeData;
    
    public static void main(String[] args){
        String help = "";
        if(args.length != 0){
            help = args[0];
        }
        
        Console s = new Console();
        try{
            
            if(args[1] != null){
               s.handleArguments(args[1]);
            }
            
        }catch(Exception e){
            System.out.println("Type --help for help");
        }
        
        if(help.equals("--help")){
            s.handleArguments(help);
        }
              
        
    }
    
     /**
     * Handles the given argument
     * @param argument 
     */
    public void handleArguments(String argument){
        Console s = new Console();
        
        
            if(argument.equals("--help")){
                s.help();
            }
            else if(argument.matches("(.)*(\\.)(gb)(--summary)(.)*")){
                s.summary(argument);
            }
            else if(argument.matches("(.)*(\\.)(gb)(--fetch_gene)(.)*")){
                s.fetchGene(argument);
            }
            else if(argument.matches("(.)*(\\.)(gb)(--fetch_cds)(.)*")){
                s.fetchCDS(argument);
            }
            else if(argument.matches("(.)*(\\.)(gb)(--fetch_features)(.)*")){
                s.fetchFeatures(argument);
            }
            else if(argument.matches("(.)*(\\.)(gb)(--find_sites)(.)*")){
                s.findSites(argument);
            }
            else{
                System.out.println("Please use: < java-jar GenBankReader --help > for help");
            }
            
        
        
             
    }
    /**
     * Prints some information if a nucleotide or amino acid sequence
     * has been found in the file
     * @param argument 
     */
    private void findSites(String argument){
        String[] arguments  = splitArguments(argument);
        Data d = execute(arguments[0]);
        SiteFinder sf = new SiteFinder();
        sf.findSites(d,arguments[1]);
        
    }
    
    
    /**
     * Prints Features
     * @param argument 
     */
    private void fetchFeatures(String argument){
        String[] arguments = splitArguments(argument);
        Data d = execute(arguments[0]);
        FeatureFinder ff = new FeatureFinder();
        ff.findFeatures(d,arguments[1]);
    }
    
    /**
     * Writes the given ID and sequence into a fasta format (+file)
     * @param ID
     * @param Sequence 
     */
    private void writeFasta(String ID,String Sequence){
        FastaWriter f = new FastaWriter();
        f.write(Sequence, ID);
        
    }
    
    
    /**
     * Gets the sequence for a given geneName if the names match, based on input arguments
     * output is a Fasta format of nucleotide sequence(s) for the matched gene(s)
     * @param argument 
     */
    public void fetchGene(String argument){
        String[] arguments = splitArguments(argument);
        Data d = execute(arguments[0]);
        String geneSequence;
        int found = 0;
        
        DataProcessor dp = new DataProcessor();
        for(int i = 0; i<d.getGeneCoords().size();i++){
            geneSequence = dp.getGeneMatch(d.getGeneNames().get(i),d.getSequences().get(0),d.getGeneCoords().get(i),arguments[1]);
            if(!geneSequence.isEmpty()){
                found +=1;
                writeFasta(d.getGeneNames().get(i),geneSequence);
            }
        }
        if(found ==0){
            System.out.println("Your gene: "+ dp.replaceJunk(arguments[1])+ " has not been found");
        }
        
    }
    
    /**
     * Gets the protein sequence if the given argument(CDS product name)
     * matches a CDS product name in the given file
     * @param argument 
     */
    public void fetchCDS(String argument){
        String[] arguments = splitArguments(argument);
        Data d = execute(arguments[0]);
        String cdsSequence;
        int found = 0;
        
        DataProcessor dp = new DataProcessor();
        for(int i = 0; i<d.getCdsCoords().size();i++){
            cdsSequence = dp.getCDSMatch(d.getCdsProduct().get(i), d.getProteinTranslation().get(i), arguments[1]);
            if(!cdsSequence.isEmpty()){
                found +=1;
                writeFasta(d.getCdsProduct().get(i),d.getProteinTranslation().get(i));
            }
        }
        if(found == 0){
            System.out.println("Your CDS product: "+ dp.replaceJunk(arguments[1])+ " has not been found");
        }
        
    }
    
    /**
     * Provides some information
     */
    public void help(){
        System.out.println("The usable commands are: \n\n"+
                    "--infile <INFILE>--summary -> for a summary of data \n"+
                    "--infile <INFILE>--fetch_gene<GENENAME(-PATTERN)> -> for nucleotide sequences of the genes that match the gene name pattern,in Fasta format \n" +
                    "--infile <INFILE>--fetch_cds<PRODUCTNAME(-PATTERN)> -> for the aminoacid sequences of the CDSs that match the product name pattern,in Fasta format \n" +
                    "--infile <INFILE>--fetch_features<COORDINATES> -> for features between coordinates\n"+
                    "--infile <INFILE>--find_sites<DNA SEQ WITH IUPAC CODES> -> lists location of all sites where the DNA pattern is found; postion and actual sequence \n");
    }
    /**
     * Gives a summary for the content of the file
     * @param argument 
     */
    public void summary(String argument){
        String[] arguments = splitArguments(argument);
        
        Data d = execute(arguments[0]);
        System.out.println("Here is some data for the file: " + arguments[0]+ "\n");
        System.out.println("Organism name: " + d.getOrganisms().get(0));
        
        System.out.println("Gene names:\tGene Coordinates: ");
        for(int o = 0; o <d.getGeneCoords().size(); o++){
            System.out.println(d.getGeneNames().get(o)+ "\t\t" + d.getGeneCoords().get(o));
        }
        
        System.out.println("\nCDS Products:\tCDS coordinates:\tdatabase xref:\t\tprotein translation: ");
        for(int x = 0; x <d.getCdsCoords().size(); x++){
            System.out.println(d.getCdsProduct().get(x) + "\t\t" + d.getCdsCoords().get(x) + "\t\t" + d.getDb_xrefs().get(x)+"\t\t" + d.getProteinTranslation().get(x));
        }
        System.out.println("Nucleotide Sequence with length of: "+d.getSequences().get(0).length()+"\n" +d.getSequences().get(0));
        System.out.println("");
    }
    
    /**
     * Performs needed actions for data
     * @param filepath
     * @return Data
     */
    private Data execute(String filepath){
       
        FileReader fr = new FileReader();
        data = fr.ReadFile(filepath);
        FileParser fp = new FileParser();
        completeData = fp.parse(data);
        Data d = new Data(completeData);
        
        return d;
        
    }
    
    /**
     * split the given arguments on "--"
     * @param argument
     * @return String[] arguments 
     */
    public String[] splitArguments(String argument){
        String [] arguments = argument.split("--");
        return arguments;
    }
    
    
}

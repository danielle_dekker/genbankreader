/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package genbankreader;

/**
 *
 * @author Dani2
 */
public class DataProcessor {
    
    /**
     * Gets the sequence for a given gene, if the names match
     * @param geneName
     * @param sequence
     * @param geneCoordinates
     * @param geneName2
     * @return String sequencePart
     */
    public String getGeneMatch(String geneName,String sequence,String geneCoordinates, String geneName2){
        if(geneName2.contains("<")){
            geneName2 = replaceJunk(geneName2);
        }
        
        String sequencePart = "";
        
        if(geneName.matches(geneName2)){
            
            String [] coordinates = geneCoordinates.split("\\..");
            int start = String2Int(coordinates[0]);
            int stop = String2Int(coordinates[1]);
            sequencePart = getSequencePart(start,stop,sequence);
            //System.out.println("Your gene: " + geneName2 + " starts at: " + start + " ends at: " + stop);
            return sequencePart;
           
            
        }
        return sequencePart;
    }
    
    /**
     * Gets the CDS translation if CDS product names match
     * @param CDSProduct
     * @param sequence
     * @param CDSProductMatch
     * @return String sequence
     */
    public String getCDSMatch(String CDSProduct, String sequence,String CDSProductMatch){
        if(CDSProductMatch.contains("<")){
            CDSProductMatch = replaceJunk(CDSProductMatch);
        }
        String mysequence = "";
        
        if(CDSProduct.matches(CDSProductMatch)){
            mysequence = sequence;
            //System.out.println("Your CDS product: " + CDSProductMatch);
            //System.out.println("Your CDS protein sequence: "+ sequence);
            return mysequence;
        }
        return mysequence;
    }
    
    /**
     * Deletes fetch_(anything) and "<>"
     * also deletes any possible "(",")","-"
     * @param anything
     * @return string
     */
    public String replaceJunk(String anything){
        
        String data = anything.replaceAll("fetch_[a-z]*<","");
        data = data.replaceAll("(\\()+(\\))+(-)*", "");
        data = data.replaceAll(">", "");
        return data;
    }
    
    /**
     * Returns a substring for given start and stop sites
     * @param start
     * @param stop
     * @param sequence
     * @return substring for given start/stop sites
     */
    private String getSequencePart(int start, int stop, String sequence){
        sequence = sequence.substring(start, stop);
        return sequence;
    }
    
    /**
     * Converts a String digit to an integer
     * @param digit
     * @return integer 
     */
    private int String2Int(String digit){
        int number = Integer.parseInt(digit);
        return number;
    }
    
}
